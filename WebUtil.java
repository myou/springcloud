/*
 * Copyright Spectra, Inc. All Rights Reserved.
 */
package spectra.ee.proxy.interconnect;

import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.*;

/**
 * Web Page 즉, JSP, Servlet에서 사용될 수 있는 기능을 가진 클래스.
 *
 * @author kspark
 */
public class WebUtil
{
    /** Logger 객체. */
    private static Logger logger = Logger.getLogger(WebUtil.class.getName());

    /** 페이지 이동을 하지 않는다.. */
    public static final String SEND_REDIRECT_NONE = "0";

    /** history.go(-1):이전페이지로 이동. */
    public static final String SEND_REDIRECT_GO_BACK = "1";

    /** 해당 페이지를 닫는다.(self.close()). */
    public static final String SEND_REDIRECT_SELF_CLOSE = "2";

    /** 해당 페이지를 닫고, opener 페이지를 reload 한다. */
    public static final String SEND_REDIRECT_SELF_CLOSE_OPENER_RELOAD = "3";

    private static String ENCODING = "UTF-8";

    /**
     * <pre>
     * 인터넷 웹 사이트 - 웹서버(HttpServer)에 요청하여 주어진 페이지의 정보를 얻어낸다.
     * 예제)
     *     String msg = WebUtil.getResponseContent(&quot;http://www.spectra.co.kr&quot;);
     *
     * GET 메소드, 디폴트 포트인 80포트를 사용하여 접속한다
     * </pre>
     *
     * @param strUrl    대상URL.
     * @param strMethod 메소드타입(GET or POST).
     * @return 결과값(responseContent).
     */
    public static String getResponseContent(String strUrl, String strMethod)
    {
        return getResponseContent(strUrl, strMethod, 60 * 1000); // 타임아웃 1분
    }

    /**
     * <pre>
     * 인터넷 웹 사이트 - 웹서버(HttpServer)에 요청하여 주어진 페이지의 정보를 얻어낸다.
     * 예제)
     *     String msg = WebUtil.getResponseContent(&quot;http://www.spectra.co.kr&quot;);
     *
     * GET 메소드, 디폴트 포트인 80포트를 사용하여 접속한다
     * </pre>
     *
     * @param strUrl    대상URL.
     * @param strMethod 메소드타입(GET or POST).
     * @param iTimeout  타임아웃설정값.
     * @return 결과값(responseContent).
     */
    public static String getResponseContent(String strUrl, String strMethod, int iTimeout)
    {
        return getResponseContent(strUrl, strMethod, iTimeout, null, null);
    }

    /**
     * HttpURLConnection이용해서 url을 호출해서 결과값을 리턴받는 함수.
     *
     * @param stringUrl   대상URL.
     * @param strMethod   메소드타입(GET or POST).
     * @param iTimeout    타임아웃설정값.
     * @param strEncoding 인코딩.
     * @param strCookies  전달할 쿠키값.
     * @return 결과값(responseContent).
     */
    public static String getResponseContent(String stringUrl, String strMethod, int iTimeout, String strEncoding, String strCookies)
    {
        URL url = null;
        HttpURLConnection httpConn = null;
        InputStream is = null;
        BufferedReader br = null;
        String str = null;
        StringBuffer msgBuffer = new StringBuffer(4 * 1024);

        try
        {
            String encoding = strEncoding;
            String strUrl = stringUrl;
            if (encoding == null || encoding.equals(""))
            {
                encoding = ENCODING;
            }

            if ("POST".equals(strMethod))
            {
                int idx = strUrl.indexOf('?');
                if (idx > -1)
                {
                    str = strUrl.substring(idx + 1);
                    strUrl = strUrl.substring(0, idx);
                }
            }

            url = new URL((URL) null, strUrl);

            if(strUrl.indexOf("https") > -1)
            {
                httpConn = (HttpsURLConnection)url.openConnection();
            }
            else
            {
                httpConn = (HttpURLConnection)url.openConnection();
            }

            httpConn.setRequestMethod(strMethod);
            httpConn.setUseCaches(false); // URL 연결의 캐시 사용여부를 설정. (default=true)
            if (strCookies != null)
            {
                httpConn.setRequestProperty("cookie", strCookies); // 쿠키값을 설정
            }

            if (str != null && strMethod.equals("POST"))
            {
                httpConn.setDoInput(true);
                httpConn.setDoOutput(true);
                httpConn.setConnectTimeout(iTimeout);
                httpConn.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=" + encoding);

                httpConn.connect();

                DataOutputStream dos = new DataOutputStream(httpConn.getOutputStream());
                dos.writeBytes(str);

                dos.flush();
                dos.close();
            }
            is = httpConn.getInputStream();
            br = new BufferedReader(new InputStreamReader(is, encoding));
            while ((str = br.readLine()) != null) // NOPMD:REVIEWED:AssignmentInOperand: by yshwang on 13. 11. 11 오후 2:25
            {
                msgBuffer.append(str);
            }
        }
        catch (InterruptedIOException e) // 타임 아웃
        {
            logger.error("WebUtil.getResponseContent - Timeout ... : " + e.getMessage() + " - " + stringUrl);
        }
        catch (MalformedURLException e)
        {
            logger.error("WebUtil.getResponseContent - Invaild Request URL ... : " + e.getMessage() + " - " + stringUrl);
        }
        catch (ProtocolException e)
        {
            logger.error("WebUtil.getResponseContent - Invaild Request Protocol ... : " + e.getMessage() + " - " + stringUrl);
        }
        catch (Exception e)
        {
            logger.error("WebUtil.getResponseContent - I/O Error ... : " + e.getMessage() + " - " + stringUrl);
        }
        finally
        {
            try
            {
                if (is != null)
                {
                    is.close();
                }
            }
            catch (Exception e)
            {
                logger.error(e);
            }
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }

        return msgBuffer.toString();
    }

    /**
     * 인터넷 웹 사이트 - 웹서버(HttpServer)에 요청하여 주어진 페이지의 정보를 얻어낸다.<br>
     * 예제)<br>
     * String msg = WebUtil.getResponseContent("http://www.spectra.co.kr");<br>
     * GET 메소드, 디폴트 포트인 80포트를 사용하여 접속한다..
     *
     * @param strUrl 대상URL.
     * @return 결과값(responseContent).
     */
    public static String getResponseContent(String strUrl)
    {
        return getResponseContent(strUrl, "GET");
    }

    /**
     * 세션에서 지정된 이름의 값이 있으면 리턴한다. 없으면 null 을 리턴한다.
     * 세션이 존재하지 않을 경우 새로운 세션을 생성하지 않는다.
     *
     * @param request 현재 HTTP 요청
     * @param name    세션 이름
     * @return 이름과 일치하는 세션의 값을 리턴하고, 없으면 null을 리턴한다.
     */
    public static Object getSessionAttribute(HttpServletRequest request, String name)
    {
        if (request == null)
        {
            throw new AssertionError("Request must not be null");
        }
        HttpSession session = request.getSession(false);
        return (session != null ? session.getAttribute(name) : null);
    }
}